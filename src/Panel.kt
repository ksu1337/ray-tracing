import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import javax.swing.JPanel

class Panel(val screen: Screen) : JPanel() {

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        val canvas = BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_ARGB)
        for (y in 0 until screen.height) {
            for (x in 0 until screen.width) {
                canvas.setRGB(x, y, screen[y, x].rgb)
            }
        }
        (g as Graphics2D).drawImage(canvas, null, null)
    }

    init {
        preferredSize = Dimension(screen.width, screen.height)
        isFocusable = true
        requestFocus()
    }

}
