import java.awt.Color

fun ambientShading(material: Material, light: DirectionLight) =  material.rateAmbient * light.ambientIntensity

fun PhongShading(cameraDirection: Vector, normal: Vector, material: Material, light: DirectionLight): Double {

    val Ia = material.rateAmbient * light.ambientIntensity

    val Id = material.rateDiffse * Math.max(0.0, cos(normal, -light.direction)) * light.diffuseIntensity

    val reflectedVector = light.direction - normal * (normal * light.direction * 2)

    val Is = material.rateSpecular * Math.max(0.0, Math.pow(cos(-cameraDirection, reflectedVector), material.reflectionAlpha)) * light.specularIntensity

    val I = Id + Is + Ia
    //return Color(Math.min(255, Math.round(material.color.red*I)).toInt(), Math.min(255, Math.round(material.color.green*I)).toInt(), Math.min(255, Math.round(material.color.blue*I)).toInt())
    return  I
//    return Math.min(1.0, Id + Is + Ia)
}