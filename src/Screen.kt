import java.awt.Color

class Screen(val width: Int, val height: Int) {

    private val buffer = Array(width * height, { Color.BLACK })

    operator fun set(y: Int, x: Int, color: Color) {
        buffer[y * width + x] = color
    }

    operator fun get(y: Int, x: Int) = buffer[y * width + x]

}