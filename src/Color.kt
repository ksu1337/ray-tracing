import java.awt.Color

operator fun Color.times(x: Double): Color {
    return Color((red * x).toInt(), (green * x).toInt(), (blue * x).toInt())
}

operator fun Color.plus(other: Color) = Color(red + other.red, green + other.green, blue + other.blue)