import java.awt.Color

fun trace(ray: Ray, depth: Int = 0): Vector {
    if (depth > 5) {
        return Vector(0.0, 0.0, 0.0)
    }
    var tMin = INFINITY
    var nearest: Figure? = null
    for (figure in scene) {
        val t = figure.intersect(ray)
        if (t < tMin) {
            tMin = t
            nearest = figure
        }
    }
    if (nearest == null) {

        return Vector(0.0, 0.0, 0.0)
    }
//    return Color.GREEN
    val intersection_point = ray.origin + ray.direction * tMin

    val object_normal = nearest.normal(intersection_point).normal()

    val ray_to_light = -light.direction

    val reflectedRay = ray.direction.normal() + object_normal * (object_normal * ray.direction.normal() * (-2))

    var color = nearest.material.color

    if (nearest is Plane) {
        if (((Math.floor(intersection_point.x / 200).toInt() + Math.floor(intersection_point.z / 200).toInt()) and 1) == 1) {
            color = Vector(1.0, 1.0, 1.0)
        } else {
            color = Vector(0.0, 0.0, 0.0)
        }
    }

    for (obj_sh in scene) {
        if (obj_sh.intersect(Ray(intersection_point + object_normal * 0.0001, ray_to_light)) < INFINITY) {
            return color * ambientShading(nearest.material, light) + trace(Ray(intersection_point + object_normal * 0.0001, reflectedRay), depth + 1) * 0.5
        }
    }
    return color * PhongShading(ray.direction, object_normal, nearest.material, light) + trace(Ray(intersection_point + object_normal * 0.0001, reflectedRay), depth + 1) * 0.5
}