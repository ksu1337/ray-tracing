import java.awt.Color

class Material(val color: Vector, val rateAmbient: Double, val rateDiffse: Double, val rateSpecular: Double, val reflectionAlpha: Double = 3.0) {
}