//class Conoid(val coordinates1: Vector, val coordinates2: Vector, val r: Double, material: Material) : Figure(material) {
//
//    fun canonization(): Pair<Matrix, Matrix> {
//        val dx = coordinates2.x - coordinates1.x
//        val dy = coordinates2.y - coordinates1.y
//        val dz = coordinates2.z - coordinates1.z
//        val r1 = Math.sqrt(Math.pow(dz, 2.0) + Math.pow(dy, 2.0))
//        val h = Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0))
//        var cosAlpha = 0.0
//        var sinAlpha = 0.0
//        if (r1 < 0.001) {
//            cosAlpha = 1.0
//            sinAlpha = 0.0
//        } else {
//            cosAlpha = dy / r1
//            sinAlpha = dz / r1
//        }
//        var cosBeta = r1 / h
//        var sinBeta = dx / h
//
//        val transferMatrix = Matrix.identity()
//
//        transferMatrix[3, 0] = -coordinates1.x
//        transferMatrix[3, 1] = -coordinates1.y
//        transferMatrix[3, 2] = -coordinates1.z
//
//        val rotateMatrixX = Matrix.identity()
//
//        rotateMatrixX[1, 1] = cosAlpha
//        rotateMatrixX[1, 2] = -sinAlpha
//        rotateMatrixX[2, 1] = sinAlpha
//        rotateMatrixX[2, 2] = cosAlpha
//
//        val rotateMatrixZ = Matrix.identity()
//
//
//        rotateMatrixZ[0, 0] = cosBeta
//        rotateMatrixZ[0, 1] = sinBeta
//        rotateMatrixZ[1, 0] = -sinBeta
//        rotateMatrixZ[1, 1] = cosBeta
//
//        val resultMatrix1 = transferMatrix * rotateMatrixX * rotateMatrixZ
//
//        rotateMatrixZ[0, 0] = cosBeta
//        rotateMatrixZ[0, 1] = -sinBeta
//        rotateMatrixZ[1, 0] = sinBeta
//        rotateMatrixZ[1, 1] = cosBeta
//
//        rotateMatrixX[1, 1] = cosAlpha
//        rotateMatrixX[1, 2] = sinAlpha
//        rotateMatrixX[2, 1] = -sinAlpha
//        rotateMatrixX[2, 2] = cosAlpha
//
//        transferMatrix[3, 0] = -coordinates1.x
//        transferMatrix[3, 1] = -coordinates1.y
//        transferMatrix[3, 2] = -coordinates1.z
//
//        val resultMatrix2 = rotateMatrixZ * rotateMatrixX * transferMatrix
//
//        return Pair(resultMatrix1, resultMatrix2)
//    }
//
//    override fun intersect(ray: Ray): Double {
//        val dx = coordinates2.x - coordinates1.x
//        val dy = coordinates2.y - coordinates1.y
//        val dz = coordinates2.z - coordinates1.z
//        val h = Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0))
//        var point = ray.origin + ray.direction
//        val (M1, М2) = canonization()
//        ray.origin = ray.origin * M1
//        point = point * M1
//        ray.direction = point - ray.origin
//        val a = Math.pow(ray.direction.x, 2.0) + Math.pow(ray.direction.z, 2.0)
//        val b = 2 * (ray.direction.x * ray.origin.x + ray.direction.z * ray.origin.z)
//        val c = Math.pow(ray.origin.x, 2.0) + Math.pow(ray.origin.z, 2.0) - Math.pow(r, 2.0)
//        val d = Math.pow(b, 2.0) - 4 * a * c
//        var result = INFINITY
//        if (a > 0.001 && d >= 0) {
//            var t = (-b - Math.pow(d, 2.0)) / 2 * a
//            if (t > 0.001) {
//                val p = (ray.origin + ray.direction) * t
//                if (p.y >= 0 && p.y <= h) {
//                    result = t
//                }
//            }
//        }
//        if (ray.direction.y < 0) {
//            var t = (h - ray.origin.y) / ray.direction.y
//            if (t > 0.001) {
//                result = t
//            }
//        } else if (ray.direction.y > 0) {
//            var t = -ray.origin.y / ray.direction.y
//            if (t > 0.001) {
//                val p = (ray.origin + ray.direction) * t
//                if (Math.pow(p.x, 2.0) + Math.pow(p.z, 2.0) <= Math.pow(r, 2.0)) {
//                    result = t
//                }
//            }
//
//        }
//        return result
//    }
//
//    override fun normal(point: Vector) = Vector(0.0, 0.0, 0.0)
//
//
//}