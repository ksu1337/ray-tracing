data class DirectionLight(var direction: Vector, val ambientIntensity: Double, val diffuseIntensity: Double, val specularIntensity: Double) {
}