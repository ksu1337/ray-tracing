fun linspace(begin: Double, end: Double, count: Int): Array<Double> {
    var result = Array<Double>(count, { 0.0 })
    var interval = (end - begin) / (count - 1)
    var current = begin
    for (i in 0 until count) {
        result[i] = current
        current += interval
    }
    return result
}