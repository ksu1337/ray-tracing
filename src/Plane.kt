class Plane(material: Material): Figure(material) {

    override fun intersect(ray: Ray): Double {
        val normal = Vector(0.0, 1.0, 0.0)
        val position = Vector(0.0, -1.0, 0.0)
        val denom = ray.direction * - normal
        if (denom > 1e-6) {
            val v = position - ray.origin
            val t = v * - normal / denom
            if (t >= 0) {
                return t
            }
        }
        return INFINITY
    }

    override fun normal(point: Vector) = Vector(0.0, 1.0, 0.0)

}




