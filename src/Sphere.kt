class Sphere(val center: Vector, val r: Double, material: Material) : Figure(material) {

    override fun intersect(ray: Ray): Double {
        val a = ray.direction.length2()
        val delta = ray.origin - center
        val b = delta * ray.direction * 2
        val c = delta.length2() - Math.pow(r, 2.0)
        val disc = b * b - 4 * a * c

        if (disc > 0) {
            val discSqrt = Math.sqrt(disc)
            val q: Double
            if (b < 0) {
                q = (-b - discSqrt) / 2.0
            } else {
                q = (-b + discSqrt) / 2.0
            }
            var t0 = q / a
            var t1 = c / q
            if (t0 > t1) {
                val buf = t0
                t0 = t1
                t1 = buf
            }
            if (t1 >= 0) {
                if (t0 < 0) {
                    return t1
                } else {
                    return t0
                }
            }
        }
//        val t = (-b + Math.sqrt(Math.pow(b, 2.0) - 4 * a * c)) / 2 * a
//        if (t > 0) {
//            return t
//        }
        return INFINITY
    }

    override fun normal(point: Vector) = (point - center).normal()

}