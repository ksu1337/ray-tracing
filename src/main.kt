import java.awt.Color
import java.util.*
import javax.swing.JFrame
import javax.swing.WindowConstants

val INFINITY = 1E30
val camera = Ray(Vector(650.0, 450.0, -1000.0), Vector(0.0, 0.0, 0.0))
val light = DirectionLight(Vector(0.5, -1.0, 0.9), 1.0, 1.0, 1.0)
val scene: Array<Figure> = arrayOf(
        Sphere(Vector(1050.0, 160.0, 800.0), 160.0, Material(Vector(0.0, 0.0, 0.9), 0.2, 0.2, 0.5)),
        Sphere(Vector(680.0, 160.0, 650.0), 160.0, Material(Vector(0.0, 0.9, 0.0), 0.2, 0.3, 0.6)),
        Sphere(Vector(310.0, 160.0, 800.0), 160.0, Material(Vector(0.9, 0.0, 0.0), 0.2, 0.2, 0.5)),
        Plane(Material(Vector(0.1, 0.1, 0.1), 0.2, 0.2, 0.3))
)

fun main(args: Array<String>) {
    val random = Random()
    light.direction = light.direction.normal()
    val screen = Screen(1367, 711)

    for (i in 0 until screen.width) {
        for (j in 0 until screen.height) {
            val ray = Ray(camera.origin, Vector(i - camera.origin.x, 599 - j - camera.origin.y, -camera.origin.z))
            val color = trace(ray, 0)
//            var color = Vector(0.0, 0.0, 0.0)
//            for (k in 0 until 4){
//                color = color + trace(Ray(ray.origin, ray.direction + Vector((random.nextDouble() - 1) , (random.nextDouble() - 1), (random.nextDouble() - 1))))
//            }
//            color = color * (1.0 / 4.0)
            screen[j, i] = Color(Math.min(color.x, 1.0).toFloat(), Math.min(color.y, 1.0).toFloat(), Math.min(color.z, 1.0).toFloat())
        }
    }
    val frame = JFrame("")
    frame.contentPane = Panel(screen)
    frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
    frame.pack()
    frame.setLocationRelativeTo(null)
    frame.isVisible = true

}










































