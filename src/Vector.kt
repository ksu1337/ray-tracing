data class Vector(var x: Double, var y: Double, var z: Double) {

    operator fun plus(other: Vector) = Vector(x + other.x, y + other.y, z + other.z)

    operator fun times(scale: Double) = Vector(x * scale, y * scale, z * scale)

    operator fun times(other: Vector) = x * other.x + y * other.y + z * other.z

    operator fun times(other: Matrix)= other * this

    operator fun unaryMinus() = Vector(-x, -y, -z)

    operator fun minus(other: Vector) = this + (-other)

    fun cross(other: Vector) = Vector(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x)

    fun length2() = x * x + y * y + z * z

    fun length() = Math.sqrt(length2())

    fun normal() = this * (1 / length())


}

fun cos(v1: Vector, v2: Vector) = v1 * v2 * 1 / v1.length() * v2.length()
