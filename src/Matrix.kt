class Matrix(val matrix: Array<Array<Double>>) {
    companion object {
        val SIZE = 4

        fun identity() = Matrix(Array(Matrix.SIZE, {
            val x = it
            Array(Matrix.SIZE, {
                val y = it
                if (x == y) 1.0 else 0.0
            })
        }))
    }

    operator fun get(y: Int, x: Int) = matrix[y][x]

    operator fun set(y: Int, x: Int, value: Double) {
        matrix[y][x] = value
    }

    operator fun times(other: Matrix) = Matrix(Array(Matrix.SIZE, {
        val i = it
        Array(Matrix.SIZE, {
            val j = it
            var result = 0.0
            for (k in 0 until Matrix.SIZE) {
                result += this[i, k] * other[k, j]
            }
            result
        })
    }))

    operator fun times(v: Vector) = Vector(
            this[0, 0] * v.x + this[0, 1] * v.x + this[0, 2] * v.x,
            this[1, 0] * v.y + this[1, 1] * v.y + this[1, 2] * v.y,
            this[2, 0] * v.z + this[2, 1] * v.z + this[2, 2] * v.z
    )

}