abstract class Figure(val material: Material) {
    abstract fun intersect(ray: Ray): Double
    abstract fun normal(point: Vector): Vector
}
